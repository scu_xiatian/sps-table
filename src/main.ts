import { createApp } from 'vue'
import App from './App'
import 'bootstrap/scss/bootstrap.scss'
import './style.scss'

createApp(App).mount('#app')
